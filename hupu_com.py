from bs4 import BeautifulSoup
import requests
from txt import txt
from time import sleep
from datetime import datetime
from datetime import date
import re
from info2mysql import info2mysql
from download import download
import signal
import functools
import sys
from os.path import exists
from os import makedirs

# 定义一个超时错误类


class TimeoutError(Exception):
    pass


def time_out(seconds, error_msg='TIME_OUT_ERROR:No connection were found in limited time!'):
    # 带参数的装饰器
    def decorated(func):
        result = ''
        # 信号机制的回调函数，signal_num即为信号，frame为被信号中断那一时刻的栈帧

        def signal_handler(signal_num, frame):
            global result
            result = error_msg
            # raise显式地引发异常。一旦执行了raise语句，raise后面的语句将不能执行
            raise TimeoutError(error_msg)

        def wrapper(*args, **kwargs):
            global result
            signal.signal(signal.SIGALRM, signal_handler)
            # 如果time是非0，这个函数则响应一个SIGALRM信号并在time秒后发送到该进程。
            signal.alarm(seconds)
            try:
                # 若超时，此时alarm会发送信息激活回调函数signal_handler，从而引发异常终止掉try的代码块
                result = func(*args, **kwargs)
            finally:
                # 假如在callback函数未执行的时候，要取消的话，那么可以使用alarm(0)来取消调用该回调函数
                signal.alarm(0)
                print('finish')
                return result
        # return wrapper
        return functools.wraps(func)(wrapper)
    return decorated


class crawler:
    # @time_out(14400)
    def main(crawl_item):
        soup = crawler.get_soup(r'https://voice.hupu.com/soccer/')
        crawler.get_item_links(soup, crawl_item)

    def get_soup(url):
        sleep(6)
        while True:
            try:
                resp = requests.get(url)
                soup = BeautifulSoup(resp.text, "lxml")
                return soup
                break
            except Exception as e:
                # print(e)
                sleep(60)
                pass

    def get_item_links(soup, crawl_item):
        items = soup.select('a.hp-set')
        for x in range(2, len(items)):
            if crawl_item == items[x].text:
                print(items[x].text)
                crawler.get_item_pages(items[x].text, items[x]['href'])

    def get_item_pages(item_name, url):
        crawler.get_page_urls(item_name, url)
        for num in range(2, 101):
            _url = re.sub(r'[.]html', r'-%s.html' % str(num), url)
            crawler.get_page_urls(item_name, _url)

    def get_page_urls(item_name, url):
        info2mysql.initDB('hupu_com', item_name)
        db_url_list = info2mysql.get_table_list(
            r'hupu_com', item_name, r'crawl_url')
        sleep(2)
        soup = crawler.get_soup(url)
        artlist = soup.select('div.tag-list-box')
        if len(artlist) > 0:
            links = artlist[0].select('span.n1 > a')
            for link in links:
                if r'https' in link['href']:
                    if len(db_url_list) > 0:
                        # avoid crawl and insert again
                        if not link['href'] in db_url_list:
                            crawler.get_txt_content(item_name, link['href'])
                            db_url_list.append(link['href'])
                        else:
                            print(r'Crawl Done !! Skip This Url: ' +
                                  link['href'])
                            # get_current_date_n_time
                            now = datetime.now()
                            current_time = now.strftime("%H:%M:%S")
                            today = date.today()
                            now_date = today.strftime("%Y-%m-%d")
                            current_data_n_time = f"{now_date} {current_time}"
                            print(current_data_n_time)
                            sleep(0.5)
                    else:
                        crawler.get_txt_content(item_name, link['href'])
                        db_url_list.append(link['href'])

    @time_out(14400)
    def get_txt_content(item_name, url):

        print('This is Page Url -----------------------------')
        print(url)
        print('This is Page Url -----------------------------')

        print(f'This is Item  -------------------------------')
        print(item_name)
        print(f'This is Item  -------------------------------')

        id_list = info2mysql.get_table_list(r'hupu_com', item_name, r'id')
        if len(id_list) > 0:
            num_list = []
            for id in id_list:
                num_list.append(int(id))
            id = int(max(num_list)) + 1
        else:
            id = 1

        sleep(2)
        soup = crawler.get_soup(url)

        post_date = soup.select('span#pubtime_baidu')
        post_date = post_date[0].text
        print(post_date)

        title = soup.select('h1')
        title = title[0].text.strip()
        title = txt.title_remove(title)
        print(title)

        artcontent = soup.select('.artical-content-read')
        imgs = artcontent[0].select('img')
        video_n_img_url_list = []
        for img in imgs:
            video_n_img_url_list.append(img['src'])
            # print(img['src'])
        download_path = r'/www/wwwroot/data/images/football'
        if not exists(download_path):
            makedirs(download_path)
        img_n_video_list = download.img_n_video(
            download_path, title, video_n_img_url_list)
        contents = str(artcontent[0]).split('<img')
        _content = ''
        x = 1
        while x < len(contents):
            content = txt.remove_html_hupu(r'<' + contents[x])
            content = txt.remove_keyword(content)
            content = content + '\n'
            _content += content
            x += 1

        db_http_imgs_list = []
        for z in img_n_video_list:
            http_imgs_list = list(z.values())
            origian_imgs_list = list(z.keys())
            db_http_imgs_list.append(http_imgs_list[0])
            print(http_imgs_list[0])
            print(origian_imgs_list[0])
            #_content = txt.txt_content_change_img_url(z, http_imgs_list, _content)
            _content = re.sub(
                origian_imgs_list[0], f'<img src="{http_imgs_list[0]}">', _content)

        print('THis is Content ------------------------------------------------------')
        print(_content)
        print('THis is Content ------------------------------------------------------')

        info2mysql.crawler2insert(r'hupu_com', item_name, id, post_date,
                                  title, r'', db_http_imgs_list, _content, item_name, url)


if __name__ == '__main__':
    while True:
        try:
            # 英超#西甲#德甲#意甲
            input_list = sys.argv
            if input_list[1] == r'英超':
                crawler.main('英超')
            elif input_list[1] == r'西甲':
                crawler.main('西甲')
            elif input_list[1] == r'德甲':
                crawler.main('德甲')
            elif input_list[1] == r'意甲':
                crawler.main('意甲')
            pass
        except Exception as err:
            print(err)
            pass
