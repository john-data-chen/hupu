#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
from txt import txt
from time import sleep
from datetime import datetime
from datetime import date
import re
from info2mysql import info2mysql
from download import download
import signal
import functools
import sys
from os.path import exists
from os import makedirs

# 定义一个超时错误类


class TimeoutError(Exception):
    pass


def time_out(seconds, error_msg='TIME_OUT_ERROR:No connection were found in limited time!'):
    # 带参数的装饰器
    def decorated(func):
        result = ''
        # 信号机制的回调函数，signal_num即为信号，frame为被信号中断那一时刻的栈帧

        def signal_handler(signal_num, frame):
            global result
            result = error_msg
            # raise显式地引发异常。一旦执行了raise语句，raise后面的语句将不能执行
            raise TimeoutError(error_msg)

        def wrapper(*args, **kwargs):
            global result
            signal.signal(signal.SIGALRM, signal_handler)
            # 如果time是非0，这个函数则响应一个SIGALRM信号并在time秒后发送到该进程。
            signal.alarm(seconds)
            try:
                # 若超时，此时alarm会发送信息激活回调函数signal_handler，从而引发异常终止掉try的代码块
                result = func(*args, **kwargs)
            finally:
                # 假如在callback函数未执行的时候，要取消的话，那么可以使用alarm(0)来取消调用该回调函数
                signal.alarm(0)
                print('finish')
                return result
        # return wrapper
        return functools.wraps(func)(wrapper)
    return decorated


class crawler:
    # @time_out(14400)
    def main(crawl_item, page_num):
        item_url_dict = {
            r'中超新闻': r'https://voice.hupu.com/china',
            r'转会新闻': r'https://voice.hupu.com/s_%E8%BD%AC%E4%BC%9A%E6%96%B0%E9%97%BB',
            r'广州恒大': r'https://voice.hupu.com/china/tag/11654.html',
            r'上海申花': r'https://voice.hupu.com/china/tag/11633.html',
            r'北京国安': r'https://voice.hupu.com/china/tag/11794.html',
            r'江苏苏宁': r'https://voice.hupu.com/china/tag/36301.html',
            r'山东鲁能': r'https://voice.hupu.com/china/tag/11676.html',
            r'上海上港': r'https://voice.hupu.com/china/tag/12136.html',
            r'广州富力': r'https://voice.hupu.com/china/tag/11793.html'
        }
        url = item_url_dict[crawl_item]
        crawler.get_article_page_url(crawl_item, url, page_num)

    def get_soup(url):
        sleep(6)
        while True:
            try:
                resp = requests.get(url)
                soup = BeautifulSoup(resp.text, "lxml")
                return soup
                break
            except Exception as e:
                # print(e)
                sleep(60)
                pass

    def get_article_page_url(crawl_item, url, page_num):
        for i in range(1, page_num+1):
            if crawl_item == r'中超新闻':
                page_url = f"{url}/{str(i)}"
            else:
                page_url = re.sub(r'.html', f"-{str(i)}.html", url)
            print(page_url)
            crawler.get_article_url(crawl_item, page_url)

    def get_article_url(crawl_item, page_url):
        info2mysql.initDB('hupu_com', crawl_item)
        db_url_list = info2mysql.get_table_list(
            r'hupu_com', crawl_item, r'crawl_url')

        soup = crawler.get_soup(page_url)
        txt_area = soup.select('.voice-main')
        articles = txt_area[0].select('.n1 a')
        if len(articles) == 0:
            txt_area = soup.select('.news-list')
            articles = txt_area[0].select('.list-hd a')
        for article in articles:
            print(article.text)
            print(article['href'])
            if len(db_url_list) > 0:
                if not article['href'] in db_url_list:
                    crawler.get_txt_content(crawl_item, article['href'])
                    db_url_list.append(article['href'])
                else:
                    print(r'Crawl Done !! Skip This Url: ' + article['href'])
                    # get_current_date_n_time
                    now = datetime.now()
                    current_time = now.strftime("%H:%M:%S")
                    today = date.today()
                    now_date = today.strftime("%Y-%m-%d")
                    current_data_n_time = f"{now_date} {current_time}"
                    print(current_data_n_time)
                    sleep(0.5)
            else:
                crawler.get_txt_content(crawl_item, article['href'])
                db_url_list.append(article['href'])

    @time_out(14400)
    def get_txt_content(crawl_item, url):

        print('This is Page Url -----------------------------')
        print(url)
        print('This is Page Url -----------------------------')

        print(f'This is Item  -------------------------------')
        print(crawl_item)
        print(f'This is Item  -------------------------------')
        info2mysql.initDB('hupu_com', crawl_item)
        id_list = info2mysql.get_table_list(r'hupu_com', crawl_item, r'id')
        if len(id_list) > 0:
            num_list = []
            for id in id_list:
                num_list.append(int(id))
            id = int(max(num_list)) + 1
        else:
            id = 1

        sleep(2)
        soup = crawler.get_soup(url)

        post_date = soup.select('span#pubtime_baidu')
        post_date = post_date[0].text
        print(post_date)

        title = soup.select('h1')
        title = title[0].text.strip()
        title = txt.title_remove(title)
        print(title)

        artcontent = soup.select('.artical-content-read')
        imgs = artcontent[0].select('img')
        video_n_img_url_list = []
        for img in imgs:
            video_n_img_url_list.append(img['src'])
            # print(img['src'])
        download_path = r'/www/wwwroot/data/images/football'
        if not exists(download_path):
            makedirs(download_path)
        img_n_video_list = download.img_n_video(
            download_path, title, video_n_img_url_list)

        contents = str(artcontent[0]).split('<img')
        _content = ''
        x = 1
        while x < len(contents):
            content = txt.remove_html_hupu(r'<' + contents[x])
            content = txt.remove_keyword(content)
            content = content + '\n'
            _content += content
            x += 1

        db_http_imgs_list = []
        for z in img_n_video_list:
            http_imgs_list = list(z.values())
            origian_imgs_list = list(z.keys())
            db_http_imgs_list.append(http_imgs_list[0])
            print(http_imgs_list[0])
            print(origian_imgs_list[0])
            #_content = txt.txt_content_change_img_url(z, http_imgs_list, _content)
            _content = re.sub(
                origian_imgs_list[0], f'<img src="{http_imgs_list[0]}">', _content)

        print('THis is Content ------------------------------------------------------')
        print(_content)
        print('THis is Content ------------------------------------------------------')
        info2mysql.initDB('hupu_com', crawl_item)
        info2mysql.crawler2insert(r'hupu_com', crawl_item, id, post_date,
                                  title, r'', db_http_imgs_list, _content, crawl_item, url)


if __name__ == '__main__':
    while True:
        try:
            input_list = sys.argv
            if input_list[1] == r'中超新闻':
                crawler.main(r'中超新闻', 1000)

            elif input_list[1] == r'转会新闻':
                crawler.main(r'转会新闻', 1000)

            elif input_list[1] == r'广州恒大':
                crawler.main(r'广州恒大', 1000)

            elif input_list[1] == r'上海申花':
                crawler.main(r'上海申花', 1000)

            elif input_list[1] == r'北京国安':
                crawler.main(r'北京国安', 1000)

            elif input_list[1] == r'江苏苏宁':
                crawler.main(r'江苏苏宁', 1000)

            elif input_list[1] == r'山东鲁能':
                crawler.main(r'山东鲁能', 1000)

            elif input_list[1] == r'上海上港':
                crawler.main(r'上海上港', 1000)

            elif input_list[1] == r'广州富力':
                crawler.main(r'广州富力', 1000)

            pass
        except Exception as err:
            print(err)
            pass
